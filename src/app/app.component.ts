import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    }
  ];


  user = {
    name: 'MBJ',
    spec: 'DEV',
    company: 'Think Tank'
  };

  menuToggled = false;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }


  ngOnInit() {
    // Hello Hafedh, how are you!!
    this.storage.set('user', JSON.stringify(this.user));

  // Or to get a key/value pair
    this.storage.get('user').then((val) => {
      console.log('user', val);
      this.user = JSON.parse(val);
      console.log('user', this.user);
    });
  }

  toggleMenu() {
   this.menuToggled = !this.menuToggled;
    console.log('toogle', this.menuToggled);
  }
}
